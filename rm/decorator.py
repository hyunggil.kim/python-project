# # -*- coding: utf-8 -*-
def decorator_function(original_function):
    def wrapper_function(*args, **kwargs):  #1
        print ('{} 함수가 호출되기전 입니다.'.format(original_function.__name__))
        print ('args: {0} // kwargs: {1}'.format(args, kwargs));
        return original_function(*args, **kwargs)  #2
    return wrapper_function


@decorator_function
def display():
    print ('display 함수가 실행됐습니다.')


@decorator_function
def display_info(*args, **kwargs):
    # print 'display_info({0}, {1}, {2}) 함수가 실행됐습니다.'.format(name, age, number)
    print(args)
    for key, value in kwargs.items():
        print("The value of {} is {}".format(key, value))

display()
print
display_info('John', 25, 333, my_name="Sammy", your_name="Casey")

#
# def print_values(*args, **kwargs):
#     print(args)
#     for key, value in kwargs.items():
#         print("The value of {} is {}".format(key, value))
#
# print_values(1, 2, 3, my_name="Sammy", your_name="Casey")