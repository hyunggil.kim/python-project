class SingletonInstance:
    _instance = None
    text = 'tttttt'

    @classmethod
    def getinstance(cls):
        return cls._instance

    @classmethod
    def instance(cls, *args, **kargs):
        cls._instance = cls(*args, **kargs)
        cls.instance = cls.getinstance
        return cls._instance

    def __init__(self):
        print('singleton init')

    def set_value(self, value):
        self.text = value

    def get_value(self):
        return self.text


# s1 = SingletonInstance.instance()
# s2 = SingletonInstance.instance()
# print('s1.get_value(): ', s1.get_value())
# print('s2.get_value(): ', s2.get_value())
# print('================================')
# print("change s1.set_value('s1')")
# s1.set_value('s1')
# print('s1.get_value(): ', s1.get_value())
# print('s1.get_value(): ', s1.get_value())
# print('================================')
# print("change s2.set_value('s2')")
# s2.set_value('s2')
# print('s1.get_value(): ', s1.get_value())
# print('s1.get_value(): ', s1.get_value())

