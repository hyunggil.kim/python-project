from signleton import SingletonInstance


class stt:
    value = 'T2'
    si = None

    def __init__(self):
        self.si = SingletonInstance.instance()

    def get_value(self):
        self.si.set_value('T2')
        return self.value + '///' + self.si.get_value()