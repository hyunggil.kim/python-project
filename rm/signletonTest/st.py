from signleton import SingletonInstance


class st:

    value = 'T1'
    si = None

    def __init__(self):
        self.si = SingletonInstance.instance()

    def get_value(self):
        return self.value + '///' + self.si.get_value()