import redis
import json


class RedisConnect:
    _instance = None
    rdb = None

    @classmethod
    def getinstance(cls):
        return cls._instance

    @classmethod
    def instance(cls, *args, **kargs):
        cls._instance = cls(*args, **kargs)
        cls.instance = cls.getinstance
        return cls._instance

    def __init__(self, host='127.0.0.1', port=6379):
        # 포트여는거 보니 여러개 만들면.. 안될것으로 판단 => 싱글톤으로 구현해야될듯.
        print('Redis Connect: {0}:{1}'.format(host, port))
        self.rdb = redis.Redis(host=host, port=port, charset='utf-8', decode_responses=True)

    def set_item(self, key, value):
        # { 회원 번호: 정보} // { key: value}
        # {'123': { 'name' : 'name', 'phone': 'phone', 'birth': 'birth' }}
        self.rdb.set(key, json.dumps(value))

    def get_item(self, key):
        data_str = self.rdb.get(key)
        if data_str:
            return {key: json.loads(data_str)}
        else:
            return {}

    def get_list(self, targetName=None):
        result = {}

        if bool(targetName):
            for key in self.rdb.keys(targetName):
                result[key] = json.loads(self.rdb.get(key))
        else:
            for key in self.rdb.keys():
                result[key] = json.loads(self.rdb.get(key))

        return result

    def delete_item(self, key):
        return self.rdb.delete(key)
