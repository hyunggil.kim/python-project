# _*_ coding: utf-8 _*_


from flask import Flask, render_template, request
from personInfoList import InfoList

app = Flask(__name__)


@app.route('/')
@app.route('/index')
@app.route('/default')
def main_page():
    infoList = InfoList()
    all_list = infoList.search_name('*')
    return render_template('default.html', all_list=all_list)


@app.route('/create_form')
def create_form():
    infoList = InfoList()
    all_list = infoList.search_name('*')
    return render_template('create_form.html', all_list=all_list)


@app.route('/search_form/<name>', methods=['GET', 'POST'])
def search_form(name='number'):
    infoList = InfoList()
    all_list = infoList.search_name('*')
    if name == 'number':
        return render_template('search_form.html', all_list=all_list, target='number')
    elif name == 'name':
        return render_template('search_form.html', all_list=all_list, target='name')
    else:
        return render_template('default.html', all_list=all_list)


@app.route('/delete_form')
def delete_form():
    infoList = InfoList()
    all_list = infoList.search_name('*')
    return render_template('delete_form.html', all_list=all_list)


@app.route('/create_info', methods=['POST'])
def create_info():
    infoList = InfoList()
    infoList.add(request.form['username'], request.form['userphone'], request.form['userbirth'])
    all_list = infoList.search_name('*')
    return render_template('default.html', all_list=all_list)


@app.route('/search/<target>', methods=['POST'])
def search_target(target):
    infoList = InfoList()
    all_list = infoList.search_name('*')

    if target == 'number':
        result_list = infoList.search_number(request.form['target'])
    elif target == 'name':
        result_list = infoList.search_name('*{0}*'.format(request.form['target']))
    else:
        result_list = []

    return render_template('search_form.html', result_list=result_list, all_list=all_list, target=target)


@app.route('/delete_number', methods=['POST'])
def delete_number():
    infoList = InfoList()
    # request.form['target']
    infoList.delete_number(request.form['target'])
    all_list = infoList.search_name('*')
    return render_template('default.html', all_list=all_list)


if __name__ == '__main__':
    app.debug = True
    app.run()
