# _*_ coding: utf-8 _*_


from redisConnect import RedisConnect


class InfoList:
    list = []
    rdb = None
    key = 'member_list'

    def __init__(self):
        self.rdb = RedisConnect.instance()

    def add(self, name, phone, birth):
        # { 회원 번호: 정보} // { key: value}
        # {'123': { 'name' : 'name', 'phone': 'phone', 'birth': 'birth' }}
        self.rdb.set_item(name+phone+birth, {'name': name, 'phone': phone, 'birth': birth})

    def search_name(self, name):
        print('name: ', name)
        return self.rdb.get_list(name)

    def search_number(self, number):
        print('number: ', number)
        return self.rdb.get_item(number)

    def delete_number(self, number):
        print('number: ', number)
        return self.rdb.delete_item(number)
