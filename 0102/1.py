# -*- coding: utf8 -*-

# 내장 함수 아무거나 10개를 사용해 코드 리뷰 => 사용처
# 1. print: 출력
print('test')
# 2. abs: 절대값
print('내장 함수 아무거나 10개를 사용해 코드 리뷰')
print('abs(-123) => ', abs(-123))
print('abs(123) => ', abs(123))
print('abs(-123) == abs(123) => ', abs(-123) == abs(123))
# 3. chr: 아스키코드에 해당하는 문자열 출력
print('chr(65) => A', chr(65))
# 4. len: 길이
a = {1: 1, 2: 2, 3: 3, 4: 4}
print('a = {1: 1, 2: 2, 3: 3, 4: 4} // len(a) => ', len(a))
a = (1, 2, 3)
len(a)
print('a = (1, 2, 3) // len(a) => ', len(a))
a = [1, 2]
len(a)
print('a = [1, 2] // len(a) => ', len(a))
a = 'hello'
len(a)
print('len(a) // len(a) => ', len(a))
# 5. round: 반올림
print('round(10.4) => ', round(10.4))
print('round(10.6) => ', round(10.6))
# 6. bool: 논리값으로 변환 / true, false
print('bool() => ', bool())
print('bool("") => ', bool(''))
print('bool([]) => ', bool([]))
# 7. dir: 객체의 어트리뷰트들을 반환, 없으면 현재 지역 기준
print('dir() => ', dir())
# 8. format: 문자열 출력
print('"test".format() => ', "test".format())
print('"test {0}".format("value") => ', "test {0}".format('value'))
# 9. range: 연속된값을 얻음
print('range(1, 10) => ', range(1, 10))
print('range(1, 10, 2) => ', range(1, 10, 2))
# 10. input: 입력값 받음
print('input("입력값: )')
input('입력값: ')
print('==================================')


# 변수 temp에 1 + 1를 하여 11로 스트링으로 변환하여 출력
print('변수 temp에 1 + 1를 하여 11로 스트링으로 변환하여 출력')
temp = str(1) + str(1)
print('temp = str(1) + str(1) => ', temp)
print('==================================')


# 변수 temp 값이 0 이면 false로 출력 0이 아니면 true로 출력하라
print('변수 temp에 1 + 1를 하여 11로 스트링으로 변환하여 출력')
temp = input("set temp: " or 0)
print('temp = input("set temp: " or 0) => ', bool(int(temp)))
print(type(temp))
print('==================================')
