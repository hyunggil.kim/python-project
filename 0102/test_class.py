#-*- coding: utf8 -*-
# - test_class.py 파일 생성후 안에 Electricity 클래스 생성
# - Electricity 클래스 안에 add 메소드를 이용해 아래 계산
# - 사용량 0~100은 단가 200원 101~300은 단가 350원 301~은 1000원
# - main.py 파일에 Electricity 클래스 객체를 생성하여
# - 전기 사용량을 입력 받아 계산해서 출력


class Electricity:
    total = 0

    def __init__(self, use = 0):
        # print('__init__ electriciry / ', use)
        self.total = use

    def add(self, use):
        # print('electricity self.use: {0}'.format(self.use))
        self.total += use

    def bill(self):
        pay = 0

        if self.total <= 100:
            pay = self.total * 200
        elif self.total <= 300:
            pay = (100 * 200) + ((self.total - 100) * 350)
        else:
            pay = (100 * 200) + (200 * 350) + ((self.total - 300) * 1000)

        return pay
