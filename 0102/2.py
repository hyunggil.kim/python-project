# -*- coding: utf8 -*-
# 근수시간 과 시급을 입력 받아 사용자 함수 리턴하여 출력하라


def func():

    while 1:
        try:
            time = int(input("근무시간: "))
            break
        except Exception as e:
            print('근무시간 입력(숫자),', e)
            continue

    while 1:
        try:
            pay = int(input("시급: "))
            break
        except Exception as e:
            print('시급 입력(숫자),', e)
            continue

    return time, pay


t, p = func()
print("근무시간: {0} // 시급: {1}".format(t, p))

# 근무시간이 40시간 초과 부터 시급 1.5배로 계산하여출력


def func2(time, pay):
    if time <= 40:
        return time * pay
    else:
        return (40 * pay) + ((time - 40) * (pay * 1.5))


print('근무시간: {0} // 시급: {1} // 총액: {2}'.format(t, p, func2(t, p)))
