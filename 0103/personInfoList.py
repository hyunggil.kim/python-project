
class InfoList:
    list = []

    def add(self, name, phone, birth):
        self.list.append({'name': name, 'phone': phone, 'birth': birth})

    def search_name(self, name):

        item = {}
        like_list = []

        for index, dic in enumerate(self.list):
            if dic['name'] == name:
                item = dic
            elif name in dic['name']:
                like_list.append(self.list[index])

        return item, like_list
