# -*- coding: utf8 -*-
# 리스트, 딕셔너리 등을 사용하여 이름, 전화번호, 생년월일을 입력받고, 이름을 검색하여 출력하는데,
#   - 정확하게 일치하는 항목
#   - 검색어를 포함하는 항목
#  을 출력한다.


from personInfoList import InfoList

il = InfoList()

while True:

    name = input('이름: ')
    phone = input('전화번호: ')
    birth = input('생년월일: ')

    if bool(name) and bool(phone) and bool(birth):
        il.add(name, phone, birth)
    else:
        print('add info fail')

    whileContinue = input('continue(y,n): ')
    if whileContinue.lower() == 'n':
        break


# il.add('n1', 'b1', 'n1')
# il.add('n2', 'b2', 'n2')
# il.add('n11', 'b11', 'n11')
# il.add('n3', 'b3', 'n3')
# il.add('n123', 'b123', 'n123')

print('\n\n')
target_name = input('검색 할 이름: ')
res1, res2 = il.search_name(target_name)

print('정확하게 일치하는 항목')
if bool(res1):
    print(' 이름: {0}\n 전화번호: {1}\n 생년월일: {2}\n'.format(res1['name'], res1['phone'], res1['birth']))
else:
    print(' 없음')

print('검색어를 포함하는 항목')
if bool(res2):
    for i in res2:
        print(' 이름: {0}\n 전화번호: {1}\n 생년월일: {2}\n'.format(i['name'], i['phone'], i['birth']))
else:
    print(' 없음')
